define(['ojs/ojcore', 'knockout', 'jquery', 'ojs/ojknockout', 'ojs/ojinputtext', 'ojs/ojselectcombobox'],
    function(oj, ko, $) {
        /**
         * The view model for the main content view template
         */
        function mainContentViewModel() {
            var self = this;
            
            self.outputText = ko.observable();

            $(document).on('textTranslated', function(e, text) {
            	self.outputText(text);
            });
        }
        
        return new mainContentViewModel();
    });
