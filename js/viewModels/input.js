/**
 * Copyright (c) 2014, 2016, Oracle and/or its affiliates.
 * The Universal Permissive License (UPL), Version 1.0
 */
/**
 * Main content module
 */
define(['ojs/ojcore', 'knockout', 'jquery',
        'ojs/ojknockout', 'ojs/ojinputtext', 'ojs/ojselectcombobox', 'ojs/ojbutton'
    ],

    function(oj, ko, $) {
        /**
         * The view model for the main content view template
         */
        function mainContentViewModel() {
            var self = this;

            self.inputText = ko.observable("Hello Oracle, how are you?");

            self.langs = ko.observableArray();
            self.valSource = ko.observableArray(["en"]);
            self.valTarget = ko.observableArray(["cs"]);

            self.translateClick = function(data, event) {
                // console.log(event.currentTarget.id);
                translate(self.inputText(), self.valSource()[0], self.valTarget()[0]);
                return true;
            }

            self.swapLangsClick = function() {
                var source = self.valSource();
                self.valSource(self.valTarget());
                self.valTarget(source);
            }

            loadLangsData(self.langs);

            $(document).on('keydown', function(e) {
                if (e.ctrlKey && e.keyCode == 13) {
                    e.preventDefault();
                    $('#btnTranslate').click();
                }
            });
        }

        function loadLangsData(langs) {
            var url = oj.Config.yandex.url + 'getLangs?key=' + oj.Config.yandex.key + '&ui=cs';

            $.getJSON(url).done(function(data) {
                for(var key in data.langs) {
                    langs.push({
                        value: key,
                        label: data.langs[key]
                    });
                }
            });
        }

        function translate(input, source, target) {
            var url = oj.Config.yandex.url + 'translate?key=' 
                + oj.Config.yandex.key 
                + '&lang=' + source + '-' + target 
                + '&text=' + encodeURIComponent(input);

            $.getJSON(url).done(function(data) {
                console.log(data);
                $(document).trigger('textTranslated', [data.text[0], input, source, target]);
            });
        }

        return new mainContentViewModel();
    });
