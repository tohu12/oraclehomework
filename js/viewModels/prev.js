define(['ojs/ojcore', 'knockout', 'jquery', 'ojs/ojknockout', 'ojs/ojinputtext', 'ojs/ojselectcombobox'],
    function(oj, ko, $) {
        /**
         * The view model for the main content view template
         */
        function mainContentViewModel() {
            var self = this;

            self.prevInputText = ko.observable(localStorage.getItem('prevInputText'));
            self.prevOutputText = ko.observable(localStorage.getItem('prevOutputText'));
            self.prevSource = ko.observable(localStorage.getItem('prevSource'));
            self.prevTarget = ko.observable(localStorage.getItem('prevTarget'));

            $(document).on('textTranslated', function(e, text, input, source, target) {
                self.prevInputText(localStorage.getItem('prevInputText'));
                self.prevOutputText(localStorage.getItem('prevOutputText'));
                self.prevSource(localStorage.getItem('prevSource'));
                self.prevTarget(localStorage.getItem('prevTarget'));

                localStorage.setItem('prevInputText', input);
                localStorage.setItem('prevOutputText', text);
                localStorage.setItem('prevSource', source);
                localStorage.setItem('prevTarget', target);
            });
        }

        return new mainContentViewModel();
    });
