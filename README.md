# ORACLE Home Work #

## Task:

Write a simple JavaScript application with [Oracle JET Framework](http://oraclejet.org/) that will:

+ Display to user a form with:
    * input text
    * source language
    * target language
    * translate button
* Send the user input to a machine translation engine (Apertium, Google translate, Microsoft translator or Yandex translate) through appropriate API
* Display the result to user
* Store the previous search to local storage. Dispaly the previous search.

## Demo:

http://www.hulek.cz/oracle_homework/

## Install:

Before need [nodejs](https://nodejs.org/), [bower](http://bower.io/) and [grunt](http://gruntjs.com/) successfully installed.

```
npm install
bower install
grunt serve
```

and open in [modern](http://browsehappy.com/) browser
[http://localhost:8000/](http://localhost:8000/)

## Description:

* Used [basic template](http://www.oracle.com/webfolder/technetwork/jet/public_samples/OracleJET_QuickStartBasic/public_html/index.html) 
* Translation engine is [Yandex](https://tech.yandex.com/translate/)
* Use GET method for call translate API => maximum size of the request string is 10 kB or browser limit
* For build release run command *"grunt build:release"*
* Unit or E2E tests missing ...